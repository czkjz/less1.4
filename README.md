# Less1.4

Создание образа:
docker build --build-arg NGINX_VER=1.0.5 -t nginxsc:v1 .

Запуск контейнера:
docker run -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf -d -p 8080:80 nginxsc:v1

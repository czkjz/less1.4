FROM debian:9 as build

ARG NGINX_VER
RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN wget "https://nginx.org/download/nginx-$NGINX_VER.tar.gz" && tar xvfz "nginx-$NGINX_VER.tar.gz" && cd "nginx-$NGINX_VER" && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY index.html ..
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
EXPOSE 80
CMD ["./nginx", "-g", "daemon off;"]

